<?php

namespace Kowal\Invoice\Controller\Adminhtml\Invoice;

use Kowal\Invoice\Service\Invoice;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;

class Pdfinvoices implements HttpGetActionInterface, HttpPostActionInterface
{
    const ADMIN_RESOURCE = 'Magento_Sales::sales_invoice';
    /**
     * @var Filter
     */
    private $filter;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var ResultFactory
     */
    private $resultFactory;
    /**
     * @var ManagerInterface
     */
    private $messageManager;
    /**
     * @var Invoice
     */
    private $invoiceService;
    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;
    /**
     * @var Filesystem\Driver\File
     */
    private $filesystem;
    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param ResultFactory $resultFactory
     * @param ManagerInterface $messageManager
     * @param Invoice $invoiceService
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param Filesystem\Driver\File $filesystem
     * @param DirectoryList $directoryList
     */
    public function __construct(
        Filter                          $filter,
        CollectionFactory               $collectionFactory,
        ResultFactory                   $resultFactory,
        ManagerInterface                $messageManager,
        Invoice $invoiceService,
        InvoiceRepositoryInterface      $invoiceRepository,
        Filesystem\Driver\File $filesystem,
        DirectoryList $directoryList

    )
    {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->resultFactory = $resultFactory;
        $this->messageManager = $messageManager;
        $this->invoiceService = $invoiceService;
        $this->invoiceRepository = $invoiceRepository;
        $this->filesystem = $filesystem;
        $this->directoryList = $directoryList;
    }

    /**
     * @param AbstractCollection $collection
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function massAction(AbstractCollection $collection)
    {
        $path = $this->directoryList->getPath(DirectoryList::TMP);
        $files = [];
        foreach ($collection->getAllIds() as $invoiceId) {
            $invoice = $this->invoiceRepository->get($invoiceId);
            $pdf = $this->invoiceService->create($invoice);
            $fileName = $invoice->getIncrementId() . '.pdf';
            $files[$fileName] = $pdf->Output($pdf::OUTPUT_RAW, '', true);
        }

        $tmpPackageName = __('Invoices') . time() . '.zip';
        $absolutePath = $path . DIRECTORY_SEPARATOR . $tmpPackageName;

        $zip = new \ZipArchive();
        $zip->open($absolutePath, \ZipArchive::CREATE);
        foreach ($files as $name => $content) {
            $zip->addFromString($name, $content);
        }
        $zip->close();

        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $result->setHeader('Content-Type', 'application/zip')
            ->setHeader('Content-disposition', 'attachment; filename=' . $tmpPackageName)
            ->setHeader('Content-Type', 'application/zip');
        $data = $this->filesystem->fileGetContents($absolutePath);
        $this->filesystem->deleteFile($absolutePath);

        $result->setContents($data);

        return $result;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            return $this->massAction($collection);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('*/*/');
        }
    }
}
