<?php

declare(strict_types=1);

namespace Kowal\Invoice\Controller\Adminhtml\Download;

use Kowal\Invoice\Service\Invoice;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\AreaList;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\Store\Model\App\Emulation;

class Pdf implements HttpGetActionInterface
{
    public const PARAM_ORDER_ID = 'invoice_id';

    public const ADMIN_RESOURCE = 'Magento_Sales::sales_invoice';

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var Invoice
     */
    private $invoice;
    /**
     * @var ResultFactory
     */
    private $resultFactory;
    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * @var Emulation
     */
    private $emulation;

    /**
     * @var AreaList
     */
    private $areaList;

    /**
     * @param RequestInterface $request
     * @param Invoice $invoice
     * @param ResultFactory $resultFactory
     * @param InvoiceRepositoryInterface $invoiceRepository
     */
    public function __construct(
        RequestInterface $request,
        Invoice $invoice,
        ResultFactory $resultFactory,
        InvoiceRepositoryInterface $invoiceRepository
    ) {
        $this->request = $request;
        $this->invoice = $invoice;
        $this->resultFactory = $resultFactory;
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);

        $id = $this->request->getParam(self::PARAM_ORDER_ID);
        $invoice = $this->invoiceRepository->get($id);

        $result->setHeader('Content-type', 'application/pdf');
        $result->setHeader('Content-Disposition', 'attachment;filename=' . $invoice->getIncrementId() . '.pdf');
        $pdf = $this->invoice->create($invoice);
        $raw = $pdf->Output($pdf::OUTPUT_RAW, '', true);
        $result->setContents($raw);

        return $result;
    }
}
