<?php

namespace Kowal\Invoice\Controller\Order;

use Kowal\Invoice\Service\Invoice;
use Magento\Customer\Model\SessionFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Controller\AbstractController\OrderViewAuthorizationInterface;

class PrintInvoice extends \Magento\Sales\Controller\Order\PrintInvoice
{
    /**
     * @var Invoice
     */
    private $invoiceService;

    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var SessionFactory
     */
    private $sessionFactory;

    /**
     * @param Context $context
     * @param OrderViewAuthorizationInterface $orderAuthorization
     * @param \Magento\Framework\Registry $registry
     * @param PageFactory $resultPageFactory
     * @param Invoice $invoiceService
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param OrderRepositoryInterface $orderRepository
     * @param SessionFactory $sessionFactory
     */
    public function __construct(
        Context $context,
        OrderViewAuthorizationInterface $orderAuthorization,
        \Magento\Framework\Registry $registry,
        PageFactory $resultPageFactory,
        Invoice $invoiceService,
        InvoiceRepositoryInterface $invoiceRepository,
        OrderRepositoryInterface $orderRepository,
        SessionFactory $sessionFactory

    ) {
        parent::__construct($context, $orderAuthorization, $registry, $resultPageFactory);
        $this->invoiceService = $invoiceService;
        $this->invoiceRepository = $invoiceRepository;
        $this->orderRepository = $orderRepository;
        $this->sessionFactory = $sessionFactory;
    }


    public function execute()
    {
        $invoiceId = (int)$this->getRequest()->getParam('invoice_id');

        if ($invoiceId) {
            return $this->getResult($invoiceId);
        }

        return parent::execute();
    }

    /**
     * @param $invoiceId
     * @return \Magento\Framework\Controller\Result\Raw|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    protected function getResult($invoiceId)
    {
        $invoice = $this->invoiceRepository->get($invoiceId);

        if ($this->orderAuthorization->canView($invoice->getOrder())) {
            $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
            $result->setHeader('Content-type', 'application/pdf');
            $result->setHeader('Content-Disposition', 'attachment;filename=' . $invoice->getIncrementId() . '.pdf');
            $pdf = $this->invoiceService->create($invoice);
            $raw = $pdf->Output($pdf::OUTPUT_RAW, '', true);
            $result->setContents($raw);

            return $result;
        }

        $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $isLogged = $this->sessionFactory->create()->isLoggedIn();
        return  $result->setPath($isLogged ? '*/*/history' : 'sales/guest/form');
    }
}
