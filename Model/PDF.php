<?php

declare(strict_types=1);

namespace Kowal\Invoice\Model;

class PDF extends \tFPDF
{
    public const OUTPUT_INLINE = 'I';
    public const OUTPUT_DOWNLOAD = 'D';
    public const OUTPUT_SAVE = 'F';
    public const OUTPUT_RAW = 'S';
}
