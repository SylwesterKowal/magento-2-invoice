<?php


declare(strict_types=1);

namespace Kowal\Invoice\Model\Container;

class Template extends \Magento\Sales\Model\Order\Email\Container\Template
{
    /**
     * @var array
     */
    protected $pdfAttachments;

    /**
     * @param array $pdfList
     * @return void
     */
    public function setPdfList(array $pdfList)
    {
        $this->pdfAttachments = $pdfList;
    }

    /**
     * @return array
     */
    public function getPdfList()
    {
        return $this->pdfAttachments;
    }
}
