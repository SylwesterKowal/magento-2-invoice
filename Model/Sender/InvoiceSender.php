<?php

declare(strict_types=1);

namespace Kowal\Invoice\Model\Sender;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Event\ManagerInterface;
use Magento\Payment\Helper\Data;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Sales\Model\Order\Email\Container\InvoiceIdentity;
use Magento\Sales\Model\Order\Email\Container\Template;
use Magento\Sales\Model\Order\Email\SenderBuilderFactory;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\ResourceModel\Order;
use Magento\Sales\Model\ResourceModel\Order\Invoice as InvoiceResource;
use Psr\Log\LoggerInterface;

class InvoiceSender extends \Magento\Sales\Model\Order\Email\Sender\InvoiceSender
{
    /**
     * @var \Kowal\Invoice\Service\Invoice
     */
    private $invoiceService;

    /**
     * @param Template $templateContainer
     * @param InvoiceIdentity $identityContainer
     * @param Order\Email\SenderBuilderFactory $senderBuilderFactory
     * @param LoggerInterface $logger
     * @param Renderer $addressRenderer
     * @param Data $paymentHelper
     * @param InvoiceResource $invoiceResource
     * @param ScopeConfigInterface $globalConfig
     * @param ManagerInterface $eventManager
     * @param \Kowal\Invoice\Service\Invoice $invoiceService
     */
    public function __construct(
        Template                        $templateContainer,
        InvoiceIdentity                 $identityContainer,
        SenderBuilderFactory            $senderBuilderFactory,
        LoggerInterface                 $logger,
        Renderer                        $addressRenderer,
        Data                            $paymentHelper,
        InvoiceResource                 $invoiceResource,
        ScopeConfigInterface            $globalConfig,
        ManagerInterface                $eventManager,
        \Kowal\Invoice\Service\Invoice  $invoiceService,
        \Kowal\Invoice\Service\Validate $validate
    )
    {
        parent::__construct(
            $templateContainer,
            $identityContainer,
            $senderBuilderFactory,
            $logger,
            $addressRenderer,
            $paymentHelper,
            $invoiceResource,
            $globalConfig,
            $eventManager
        );

        $this->invoiceService = $invoiceService;
        $this->validate = $validate;
    }

    /**
     * @param Invoice $invoice
     * @param $forceSyncMode
     * @return bool
     * @throws \Exception
     */
    public function send(Invoice $invoice, $forceSyncMode = false)
    {
        $invoice->setSendEmail($this->identityContainer->isEnabled());

        if (!$this->globalConfig->getValue('sales_email/general/async_sending') || $forceSyncMode) {
            $order = $invoice->getOrder();
            $this->identityContainer->setStore($order->getStore());




            if ($this->checkIfPartialInvoice($order, $invoice)) {
                $order->setBaseSubtotal((float)$invoice->getBaseSubtotal());
                $order->setBaseTaxAmount((float)$invoice->getBaseTaxAmount());
                $order->setBaseShippingAmount((float)$invoice->getBaseShippingAmount());
            }

            $transport = [
                'order' => $order,
                'order_id' => $order->getId(),
                'invoice' => $invoice,
                'invoice_id' => $invoice->getId(),
                'comment' => $invoice->getCustomerNoteNotify() ? $invoice->getCustomerNote() : '',
                'billing' => $order->getBillingAddress(),
                'payment_html' => $this->getPaymentHtml($order),
                'store' => $order->getStore(),
                'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
                'formattedBillingAddress' => $this->getFormattedBillingAddress($order),
                'order_data' => [
                    'customer_name' => $order->getCustomerName(),
                    'is_not_virtual' => $order->getIsNotVirtual(),
                    'email_customer_note' => $order->getEmailCustomerNote(),
                    'frontend_status_label' => $order->getFrontendStatusLabel()
                ]
            ];
            $transportObject = new DataObject($transport);

            /**
             * Event argument `transport` is @deprecated. Use `transportObject` instead.
             */
            $this->eventManager->dispatch(
                'email_invoice_set_template_vars_before',
                ['sender' => $this, 'transport' => $transportObject->getData(), 'transportObject' => $transportObject]
            );

            $this->templateContainer->setTemplateVars($transportObject->getData());

            $billingAddress = $order->getBillingAddress();
            $nip = $billingAddress->getVatId();

            if($enabled = $this->globalConfig->isSetFlag('invoicepdf/settings/enable', ScopeInterface::SCOPE_STORE, $order->getStoreId())) {
                $validate = $this->globalConfig->getValue(
                    'invoicepdf/settings/validate',
                    ScopeInterface::SCOPE_STORE,
                    $order->getStoreId()
                );
                $sendPdf = ($validate) ? $this->validate->Nip($nip) : true;
                if ($sendPdf) {
                    $invoicePdf = $this->invoiceService->create($invoice);
                    $invoiceContent = $invoicePdf->Output('S', '', true);
                    $this->templateContainer->setPdfList([
                        $invoice->getIncrementId() . '.pdf' => $invoiceContent
                    ]);
                }
            }
            if ($this->checkAndSend($order)) {
                $invoice->setEmailSent(true);
                $this->invoiceResource->saveAttribute($invoice, ['send_email', 'email_sent']);
                return true;
            }
        } else {
            $invoice->setEmailSent(null);
            $this->invoiceResource->saveAttribute($invoice, 'email_sent');
        }

        $this->invoiceResource->saveAttribute($invoice, 'send_email');

        return false;
    }

    /**
     * Check if the order contains partial invoice
     *
     * @param \Magento\Sales\Model\Order $order
     * @param Invoice $invoice
     * @return bool
     */
    private function checkIfPartialInvoice($order, $invoice): bool
    {
        $totalQtyOrdered = (float)$order->getTotalQtyOrdered();
        $totalQtyInvoiced = (float)$invoice->getTotalQty();
        return $totalQtyOrdered !== $totalQtyInvoiced;
    }
}
