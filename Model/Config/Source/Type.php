<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Invoice\Model\Config\Source;

class Type implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'VAT Invoice', 'label' => __('VAT Invoice')],['value' => 'Pro Forma Invoice', 'label' => __('Pro Forma Invoice')]];
    }

    public function toArray()
    {
        return ['VAT InvoiceT' => __('VAT Invoice'),'Pro Forma Invoice' => __('Pro Forma Invoice')];
    }
}
