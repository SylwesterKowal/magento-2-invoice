<?php
declare(strict_types=1);

namespace Kowal\Invoice\Service;

use Kowal\Invoice\Model\PDF;
use Magento\Framework\App\Area;
use Magento\Framework\App\AreaInterface;
use Magento\Framework\App\AreaList;
use Magento\Sales\Api\Data\InvoiceInterface;
use Magento\Store\Model\App\Emulation;

class Invoice
{
    /**
     * @var Generator
     */
    private $generator;
    /**
     * @var Emulation
     */
    private $emulation;

    /**
     * @var AreaList
     */
    private $areaList;

    /**
     * @param Generator $generator
     * @param Emulation $emulation
     * @param AreaList $areaList
     */
    public function __construct(
        Generator $generator,
        GeneratorRmg $generatorRmg,
        Emulation $emulation,
        AreaList $areaList
    ) {
        $this->generator = $generator;
        $this->generatorRmg = $generatorRmg;
        $this->emulation = $emulation;
        $this->areaList = $areaList;
    }

    /**
     * @param InvoiceInterface $invoice
     * @return PDF|null
     */
    public function create(InvoiceInterface $invoice): ?PDF
    {

        $this->emulation->startEnvironmentEmulation($invoice->getStoreId(), Area::AREA_FRONTEND, true);
        $area = $this->areaList->getArea(Area::AREA_FRONTEND);
        $area->load(AreaInterface::PART_TRANSLATE);

        if(!is_null($invoice->getOrder()->getCouponCode())) {
            $cuponCodePrefix = substr((string)$invoice->getOrder()->getCouponCode(), 0, 2);
        }else{
            $cuponCodePrefix = false;
        }

        if($invoice->getStoreId() == 16 && $cuponCodePrefix == 'SM') { // zmienieć na 20 dla dev
            $pdf = $this->generatorRmg->build($invoice);
        }else {
            $pdf = $this->generator->build($invoice);
        }
        $this->emulation->stopEnvironmentEmulation();

        return $pdf;
    }
}
