<?php

declare(strict_types=1);

namespace Kowal\Invoice\Service;

use Kowal\Invoice\Model\PDF;
use Kowal\Invoice\Service\CountriesTax;
use KwotaSlownie\KwotaSlownie;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Sales\Api\Data\InvoiceInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Tax\Api\TaxCalculationInterface;

class Generator
{
    /**
     * @var PDF
     */
    private $page;

    /**
     * @var InvoiceInterface
     */
    private $invoice;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var TaxCalculationInterface
     */
    private $taxCalculation;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param TaxCalculationInterface $taxCalculation
     */
    public function __construct(
        ScopeConfigInterface    $scopeConfig,
        TaxCalculationInterface $taxCalculation,
        CountriesTax            $countriesTax
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->taxCalculation = $taxCalculation;
        $this->countriesTax = $countriesTax;
    }

    /**
     * @return void
     */
    public function createPage()
    {

        $this->page = new PDF();
        $this->page->AddPage('P', 'A4');
        $this->page->AddFont('DejaVu', '', 'DejaVuSansCondensed.ttf', true);
        $this->page->AddFont('DejaVu-Bold', '', 'DejaVuSansCondensed-Bold.ttf', true);
        $this->page->SetFont('DejaVu', '', 10);
    }

    /**
     * @param InvoiceInterface $invoice
     * @return void
     */
    public function setInvoice(InvoiceInterface $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @param InvoiceInterface $invoice
     * @return PDF|null
     */
    public function build(InvoiceInterface $invoice)
    {
        $this->createPage();
        $this->setInvoice($invoice);
        $this->drawHeader();
        $this->drawAddress();
        $this->drawItems();
        $this->drawFooter();

        return $this->getPage();
    }

    /**
     * @return PDF|null
     */
    public function getPage(): ?PDF
    {
        return $this->page;
    }

    /**
     * @return void
     */
    public function drawFooter()
    {

        $this->page->Cell(10, 10, '', 0, 1);
        $this->page->SetFont('DejaVu-Bold', '', 9);
        $this->page->Cell($this->page->GetPageWidth() - 20, 10,
            __(' '),
            0, 1, 'C');
        $this->page->SetFont('DejaVu', '', 9);

        $paidAmount = $this->formatNumber($this->invoice->getGrandTotal()).' '. $this->invoice->getOrderCurrencyCode();
        $toPay = $this->formatNumber(0).' '. $this->invoice->getOrderCurrencyCode();

        $this->page->Cell(100);
        $this->page->Cell(100, 10, __('Amount paid: %1', $paidAmount), 0, 1);
        $this->page->Line(10, $this->page->GetY() - 2, $this->page->GetPageWidth() - 10, $this->page->GetY() - 2);
        $this->page->Cell(100);
        $this->page->Cell(100, 5, __('To pay: %1', $toPay), 0, 1);
        $this->page->Cell(100);
//        $this->page->Cell(100, 4, __('in words'), 0, 1);

//        $amount = new KwotaSlownie($this->invoice->getGrandTotal());
//        $this->page->Cell(180, 5, $amount, 0, 1, 'R');

        $spaceLeft = $this->page->GetPageHeight() - $this->page->GetY();

        $this->page->Cell(10, $spaceLeft - 40, '', 0, 1);
        $this->page->Cell($this->page->GetPageWidth() - 20, 10,
            __('An invoice as an electronic document does not require a signature in accordance with art. 2 paragraph 32 and article 106e. The VAT Act.'),
            0, 1);
    }

    /**
     * @return DataObject
     */
    private function getDeliveryItem()
    {
        $item = new DataObject();

        $item->setData('sku', '');
        $item->setData('name', __('Shipping'));
        $item->setData('qty_invoiced', 1);
        $item->setData('price', $this->invoice->getShippingAmount());
        $item->setData('row_total', $this->invoice->getShippingAmount());
        $item->setData('row_total_incl_tax', $this->invoice->getShippingInclTax());
        $item->setData('price_incl_tax', $this->invoice->getShippingInclTax());
        $item->setData('tax_amount', $this->invoice->getShippingTaxAmount());

        $taxClassId = $this->scopeConfig->getValue('tax/classes/shipping_tax_class', ScopeInterface::SCOPE_STORE, $this->invoice->getStoreId());
        $item->setData('tax_percent', $this->getTaxPercent($taxClassId, $this->invoice->getOrder()));

        return $item;
    }

    /**
     * @param $taxClassId
     * @param OrderInterface $order
     * @return float|int
     */
    private function getTaxPercent($taxClassId, $order)
    {
        $tax = $this->taxCalculation->getCalculatedRate(
            $taxClassId,
            $order->getCustomerId(),
            $order->getStoreId()
        );

        return $this->formatNumber($tax);
    }

    /**
     * @return void
     */
    public function drawItems()
    {


        $number = 1;

        /**
         * @var OrderInterface $order
         */
        $order = $this->invoice->getOrder();

        $taxType = "standard";
        $billingAddress = $order->getBillingAddress();
        $countryId = is_null($order->getShippingAddress()) ? "" : $order->getShippingAddress()->getCountryId();
        $stawkaVat = (int)$this->countriesTax->getTaxRate($taxType, $countryId);


        $this->page->Cell(10, 10, __('No.'), 0, 0, 'C', 0);
        $x = $this->page->GetX();
        $y = $this->page->GetY();
        $this->page->Cell(100, 5, __(''));
        $this->page->Text($x + 5, $y + 6, __('Product Code'));

        $this->page->Cell(15, 10, __('Qty/unit'), 0, 0, 'C', 0);

        $x = $this->page->GetX();
        $y = $this->page->GetY();
        $this->page->Cell(20, 5, __('Price'), 0, 0, 'C', 0);
        $this->page->Text($x + 6, $y + 6, strtolower((string)__('Gross')));

        $x = $this->page->GetX();
        $y = $this->page->GetY();
        $this->page->Cell(25, 5, __('Amount'), 0, 0, 'C', 0);
        $this->page->Text($x + 10, $y + 6, strtolower((string)__('Gross')));
        $this->page->Cell(20, 10, __('TAX'), 0, 0, 'C', 0);
        $this->page->Rect(10, 102, 190, 10);
        $this->page->Cell(1, 10, '', 0, 1);

        $summary = [];

        $allItems = $order->getAllVisibleItems();

        $allItems[] = $this->getDeliveryItem();
        /**
         * @var OrderItemInterface $item
         */
        foreach ($allItems as $item) {
            $this->page->Cell(10, 10, $number++, 0, 0, 'C', 0);
            $x = $this->page->GetX();
            $y = $this->page->GetY();

            $this->page->Cell(100,5, $item->getSku());
            $this->page->Text($x + 5, $y + 7, $item->getName());

            $this->page->Cell(15, 10, $this->formatNumber($item->getQtyInvoiced()), 0, 0, 'C', 0);

            $this->page->Cell(20, 10, $this->formatNumber($item->getPrice() - ($item->getDiscountAmount() / $item->getQtyInvoiced() )), 0, 0, 'C', 0);

            $this->page->Cell(25, 10, $this->formatNumber($item->getRowTotal() -    $item->getDiscountAmount()   ), 0, 0, 'C', 0);

            $tax = $this->formatNumber($stawkaVat);

            $this->page->Cell(20, 10, $tax . '%', 0, 0, 'C', 0);
            $this->page->Cell(1, 8, '', 0, 1);

            if (!isset($summary[$tax])) {
                $summary[$tax] = [
                    'gross' => 0,
                    'tax' => 0
                ];
            }
            $priceAfterDiscount = $item->getRowTotalInclTax()  - $item->getDiscountAmount() ;
            $summary[$tax]['gross'] += ($priceAfterDiscount);
            $summary[$tax]['tax'] += $priceAfterDiscount - $this->calcNetPrice($priceAfterDiscount, $tax);

        }

        $this->page->Cell(1, 2, '', 0, 1);
        $this->page->cell(100, 5);
        $this->page->cell(15, 5, '', 0, 0, '', true);
        $this->page->cell(15, 5, __(''), 0, 0, '', true);
        $this->page->cell(20, 5, __('Net'), 0, 0, '', true);
        $this->page->cell(20, 5, __('TAX'), 0, 0, '', true);
        $this->page->cell(20, 5, __('Gross'), 0, 1, '', true);

        $this->page->cell(100, 5);
        $this->page->SetFont('DejaVu-Bold', '', 10);
        $this->page->cell(15, 5, __('Total'), 0);
        $this->page->SetFont('DejaVu', '', 9);


        $summaryNet = array_map(function ($item) {
            return $item['gross'] - $item['tax'];
        }, $summary);

        $summaryTax = array_map(function ($item) {
            return $item['tax'];
        }, $summary);

        $summaryGross = array_map(function ($item) {
            return $item['gross'] ;
        }, $summary);

        $this->page->cell(15, 5, '', 0);
        $this->page->cell(20, 5, $this->formatNumber(array_sum($summaryNet)), 0);
        $this->page->cell(20, 5, $this->formatNumber(array_sum($summaryTax)), 0);
        $this->page->cell(20, 5, $this->formatNumber(array_sum($summaryGross)).' '. $this->invoice->getOrderCurrencyCode(), 0, 1);
        $this->page->Line(110, $this->page->GetY(), $this->page->GetPageWidth() - 10, $this->page->GetY());

        $firstSummaryRow = true;
        foreach ($summary as $vat => $line) {
            $this->page->cell(100, 5);

            if ($firstSummaryRow) {
                $this->page->SetFont('DejaVu-Bold', '', 10);
                $this->page->cell(15, 5, __('VAT rate'), 0);
                $this->page->SetFont('DejaVu', '', 9);
                $firstSummaryRow = false;
            } else {
                $this->page->cell(15, 5, '', 0);
            }

            $this->page->cell(15, 5, $vat . '%', 0);
            $this->page->cell(20, 5, $this->formatNumber($line['gross'] - $line['tax']), 0);
            $this->page->cell(20, 5, $this->formatNumber($line['tax']), 0);
            $this->page->cell(20, 5, $this->formatNumber($line['gross']).' '. $this->invoice->getOrderCurrencyCode(), 0, 1);
            $this->page->Line(110, $this->page->GetY(), $this->page->GetPageWidth() - 10, $this->page->GetY());
        }


//        $this->drawDiscount($this->invoice->getGrandTotal());
    }

    /**
     * @param $price
     * @param $tax
     * @return float|int
     */
    private function calcNetPrice($price, $tax)
    {
        return $price * 100 / ($tax + 100);
    }

    /**
     * @param $totalPrice
     * @return void
     */
    private function drawDiscount($totalPrice)
    {
        $totalPrice = $this->formatNumber($totalPrice);
        $totalFromOrder = $this->formatNumber($this->invoice->getGrandTotal());

        if ($totalPrice == $totalFromOrder) {
            return;
        }
        $this->page->cell(90, 2, '', 0, 1);
        $this->page->cell(90, 5);
        $this->page->SetFont('DejaVu-Bold', '', 10);
        $this->page->cell(15, 5, __('After Discount'), 0);
        $this->page->SetFont('DejaVu', '', 9);

        $this->page->cell(25, 5, '', 0);
        $this->page->cell(20, 5, $this->formatNumber($this->invoice->getGrandTotal() - $this->invoice->getTaxAmount()), 0);
        $this->page->cell(20, 5, $this->formatNumber($this->invoice->getTaxAmount()), 0);
        $this->page->cell(20, 5, $this->formatNumber($this->invoice->getGrandTotal()), 0, 1);
        $this->page->SetFillColor(0, 0, 0);
        $this->page->Rect(100, $this->page->GetY(), 100, 1, 'F');
        $this->page->SetFillColor(200, 200, 200);
    }

    /**
     * @return void
     */
    private function drawAddress()
    {
        $address = $this->invoice->getOrder()->getBillingAddress();

        $this->page->Cell(5);
        $this->page->Cell(84, 5, __('Buyer') . ':');
        $this->page->Cell(12);
        $this->page->Cell(94, 5, __('Recipient') . ':', 0, 1);
        $vatId = $address->getVatId();

        $lines = [
            $address->getFirstname() . ' ' . $address->getLastname(),
            $address->getCompany(),
            implode(' ', $address->getStreet()) . ', ' . $address->getPostcode() . ' ' . $address->getCity(),
            $vatId ? __('Tax ID %1', $vatId) : ''
        ];
        if ($vatId) {
            array_shift($lines);
        }

        foreach ($lines as $line) {
            $this->page->Cell(10);
            $this->page->Cell(79, 5, $line);
            $this->page->Cell(17);
            $this->page->Cell(89, 5, $line, 0, 1);
        }
        $shippingMethod = $this->invoice->getOrder()->getShippingDescription();

        $this->page->Cell(30, 10, __('Delivery method') . ':');
        $this->page->Cell(200, 10, $shippingMethod, 0, 1);

        $order = $this->invoice->getOrder();
        $payment = $order->getPayment()->getAdditionalInformation();
        $payment_title = (isset($payment['method_title'])) ? $payment['method_title'] : "";
        $this->page->Cell(1, 5, __('Payment method: %1', $payment_title ), 0, 1);


        // $this->page->Cell(200, 10, __('Payment method: %1', $payment['method_title']), 0, 1);
    }

    /**
     * @return void
     */
    private function drawHeader()
    {
        $order = $this->invoice->getOrder();

        $invoiceAddressValues = explode(
            PHP_EOL,
            (string)$this->scopeConfig->getValue(
                'sales/identity/address',
                ScopeInterface::SCOPE_STORE,
                $this->invoice->getStoreId()
            )
        );

        $storeName = trim($invoiceAddressValues[0] ?? '');
        $addressLine1 = trim($invoiceAddressValues[1] ?? '');
        $addressLine2 = trim($invoiceAddressValues[2] ?? '');
        $taxIdInfo = trim($invoiceAddressValues[3] ?? '');

        $this->page->SetFont('DejaVu', '', 9);
        $this->page->Cell(94, 5, __('Seller'), 0);
        $this->page->Cell(2);
        $this->page->SetFillColor(200, 200, 200);
        $this->page->SetFont('DejaVu-Bold', '', 10);
        $this->page->Cell(94, 5, __($this->scopeConfig->getValue(
            'invoicepdf/settings/type',
            ScopeInterface::SCOPE_STORE,
            $this->invoice->getStoreId()
        )), 0, 1, 'C', 1);
        $this->page->SetFont('DejaVu', '', 9);
        $this->page->Cell(94, 5, '', 0);
        $this->page->Cell(2);
        $this->page->Cell(94, 5, __('invoice no.: %1', $this->invoice->getIncrementId()), 0, 1, 'C');

        $this->page->Cell(10);
        $this->page->Cell(84, 5, $storeName);
        $this->page->Cell(2);
        $this->page->Cell(94, 5, __('order no.: %1', $this->invoice->getOrder()->getIncrementId()), 0, 1, 'C');

        $this->page->Cell(10);
        $this->page->Cell(84, 5, $addressLine1);
        $this->page->Cell(2);
        $this->page->Cell(94, 5, '', 0, 1);


        $formattedDate = date('d-m-Y', strtotime($this->invoice->getCreatedAt()));
        $this->page->Cell(10);
        $this->page->Cell(84, 5, $addressLine2);
        $this->page->Cell(2);
        $this->page->Cell(54, 5, __('Date of issue'));
        $this->page->SetFont('DejaVu-Bold', '', 9);
        $this->page->Cell(40, 5, $formattedDate, 0, 1, 'R');
        $this->page->SetFont('DejaVu', '', 9);

        $this->page->Cell(10);
        $this->page->Cell(84, 5, $taxIdInfo);
        $this->page->Cell(2);
        $this->page->Cell(94, 5, '', 0, 1);

        $this->page->Cell(94, 5, '');
        $this->page->Cell(2);
        $this->page->Cell(54, 5, __(' '));
        $this->page->SetFont('DejaVu-Bold', '', 9);
        $this->page->Cell(40, 5, ' ', 0, 1, 'R');
        $this->page->SetFont('DejaVu', '', 9);

        $this->page->SetFillColor(0, 0, 0);
        $this->page->Rect(10, 10, 90, 40, 'D');
        $this->page->Rect(100, 12, 1, 39, 'F');
        $this->page->Rect(11, 50, 89, 1, 'F');
        $this->page->SetFillColor(200, 200, 200);
        $cellHeight = $order->getBillingAddress()->getVatId() ? 17 : 12;

        $this->page->Cell(200, $cellHeight, '', 0, 1);
        $this->page->Cell(35, 5, ' ', 0);
        $this->page->Cell(150, 5, ' ', 0, 1);


    }

    /**
     * @param $amount
     * @return string
     */
    private function formatNumber($amount)
    {
        return number_format((float)$amount, 2, '.', '');
    }
}
