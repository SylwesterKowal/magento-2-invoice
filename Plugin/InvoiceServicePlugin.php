<?php

declare(strict_types=1);

namespace Kowal\Invoice\Plugin;

use Magento\Sales\Model\Service\InvoiceService;

class InvoiceServicePlugin
{

    /**
     * Set invoice date equal to order date
     *
     * @param InvoiceService $subject
     * @param $invoice
     * @return mixed
     */
    public function afterPrepareInvoice(InvoiceService $subject, $invoice)
    {
        $order = $invoice->getOrder();
        $invoice->setCreatedAt($order->getCreatedAt());

        return $invoice;
    }
}
