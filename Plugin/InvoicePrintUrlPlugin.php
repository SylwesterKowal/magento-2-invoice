<?php

declare(strict_types=1);

namespace Kowal\Invoice\Plugin;

use Magento\Sales\Block\Adminhtml\Order\Invoice\View;

class InvoicePrintUrlPlugin
{

    /**
     * @param View $subject
     * @param $result
     * @return string
     */
    public function afterGetPrintUrl(View $subject, $result)
    {
        return $subject->getUrl('kowal_invoice/download/pdf', [
            'invoice_id' => $subject->getInvoice()->getId()
        ]);
    }
}
