<?php

declare(strict_types=1);

namespace Kowal\Invoice\ViewModel;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class PrintInvoice implements ArgumentInterface
{
    /**
     * @var UrlInterface
     */
    private UrlInterface $urlInterface;

    /**
     * @param UrlInterface $urlInterface
     */
    public function __construct(
        UrlInterface $urlInterface
    ) {
        $this->urlInterface = $urlInterface;
    }

    /**
     * @param $invoice
     * @return string
     */
    public function getPrintInvoiceUrl($invoice)
    {
        return $this->urlInterface->getUrl('sales/order/printInvoice', ['invoice_id' => $invoice->getId()]);
    }
}
